<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url' => 'https://landing.e.multidelo.com/' . rand(1, 20),
            'datetime' => $this->faker->dateTime,
        ];
    }
}
