# Проект Activity

Основан на Laravel 8.*

URL: [https://activity.e.multidelo.com](https://activity.e.multidelo.com)

Использует https://github.com/sajya/server для обработки ответов в формате JSON-RPC 2.0

Для дебага и отладки осознанно активирован глобально Laravel [Telescope](https://laravel.com/docs/8.x/telescope#introduction). На демо доступен по адресу: [https://activity.e.multidelo.com/telescope/](https://activity.e.multidelo.com/telescope/). Также осознанно у проекта на демо сделано окружение local для проверки и тестирования


## Локальная установка и запуск проекта

```bash
git clone https://gitlab.com/vsyaco/e-activity.git
cd e-activity
composer install
cp .env.example .env
# Указать в .env IP-адрес landing'a в CLIENT_IP
./vendor/bin/sail up -d
./vendor/bin/sail php artisan migrate
```

Можно заполнить базу фейковыми запросами:
```bash
./vendor/bin/sail php artisan db:seed
```

## Ключевые места проекта
- **app/Http/Procedures/ActivityProcedure.php** - контроллер json-rpc 2.0 процедур
- **app/Http/Middleware/Restrict.php** - глобальный middleware для ограничения доступа
- **app/Http/Requests/StoreActivityRequest.php** - валидация сохраняемых данных из запроса визита
- **app/Http/routes/api.php** - роут ендпоинта для запросов JSON-RPC 2.0
- **app/Models/Activity.php** - модель Активности
- **database/factories/ActivityFactory.php** - заполнение фейковыми данными (вызов из **database/seeders/DatabaseSeeder.php**)
