<?php

declare(strict_types=1);

namespace App\Http\Procedures;

use App\Http\Requests\StoreActivityRequest;
use App\Models\Activity;
use Illuminate\Support\Facades\DB;
use Sajya\Server\Procedure;

class ActivityProcedure extends Procedure
{
    /**
     * The name of the procedure that will be
     * displayed and taken into account in the search
     *
     * @var string
     */
    public static string $name = 'activity';

    /**
     * Add new visit.
     *
     * @return string
     */
    public function add(StoreActivityRequest $request)
    {
        return Activity::create($request->validated())->id;
    }

    /**
     * Get visits.
     *
     //* @return string
     */
    public function get()
    {
        //Only for no strict MySQL mode
        //        return Activity::select(DB::raw('url, datetime'), DB::raw('count(*) as total'))
        //            ->groupBy('url')
        //            ->orderBy('datetime', 'desc')
        //            ->paginate(5)->withPath('');

        //Clean PostgresSQL and MySQL
        //SELECT m.*, total
        //FROM activities m
        //    LEFT JOIN activities b
        //        ON m.url = b.url
        //    AND m.datetime < b.datetime
        //    LEFT JOIN (SELECT url, count(*) as total FROM activities GROUP by url) as c on m.url = c.url
        //WHERE b.datetime IS NULL
        //ORDER by datetime DESC

        //For PostgresSQL and MySQL Query builder

        return DB::table('activities')
            ->select('activities.*', 'total')
            ->leftJoin(DB::raw('activities as b'),
                function($join)
                {
                    $join->on('activities.url', '=', 'b.url');
                    $join->on('activities.datetime', '<', 'b.datetime');
                })
            ->leftJoin(DB::raw('(SELECT url, count(*) as total FROM activities GROUP by url) as c'),
                function($join)
                {
                    $join->on('activities.url', '=', 'c.url');
                })
            ->whereNull('b.datetime')
            ->orderBy('datetime', 'DESC')->paginate(5);


    }
}
